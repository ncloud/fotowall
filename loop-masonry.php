	
	<div class="width-sizer"></div>
	<div class="gutter-sizer"></div>

<?php while ( have_posts() ) {
		the_post();

		$classes = array();
		if(has_post_thumbnail()) $classes[] = 'have_thumbnail';
		else $classes[] = 'not_have_thumbnail';
?>
	<div <?php post_class($classes); ?>>
		<div class="post_content">
  		<?php 
  			if ( has_post_thumbnail() ) {
  		?>
  				<div class="thumbnail_wrap">
  					<?php the_post_thumbnail(array(280, 9999));?>
  					<a href="<?php the_permalink();?>"></a>
				</div>
		<?php

			} else {
		?>
				<div class="inverse">
		<?php
			}
		?>
			<?php the_title( '<h3><a href="' . get_permalink().'">', '</a></h3>' ); ?>
			<div class="categories"><?php the_category(', '); ?></div>
		<?php
			if( !has_post_thumbnail()) {
		?>
				</div>
		<?php
			}
		?>
		</div>
    </div>
<?php
	}
?>

<!--
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/scripts/jquery.tile.js';?>"></script>
<script type="text/javascript">
	var $post_content = jQuery('#post_content');

	//$post_content.imagesLoaded(function() {
	    $post_content.tile({
	        itemSelector : '.post',
	        columnWidth : 266, 
	        gutterWidth : 22
	    });
	//});
</script>-->

<script type="text/javascript" src="<?php echo get_template_directory_uri().'/scripts/jquery.masonry.js';?>"></script>
<script type="text/javascript">
	var $post_content = jQuery('#post_content');
	$post_content.imagesLoaded( function() {
		$post_content.masonry({
		  columnWidth: '.width-sizer',
		  gutter: '.gutter-sizer',
		//  isFitWidth: true, 
		  isAnimated: false,
		  isInitLayout: false,
		  itemSelector: '.post',
		});

		$post_content.masonry( 'on', 'layoutComplete', function( msnryInstance, laidOutItems ) {

			return true;
		});	

		$post_content.masonry('layout');
	});
</script>
