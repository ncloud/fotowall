<?php

define( 'FOTOWALL_DIR', TEMPLATEPATH );
define( 'FOTOWALL_LIBRARY', FOTOWALL_DIR . '/library' );

include_once(FOTOWALL_LIBRARY.'/thumbnail.php');
include_once(FOTOWALL_LIBRARY.'/class.php');
include_once(FOTOWALL_LIBRARY.'/func.php');


// init
function fotowall_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'Fotowall' ) );
	
	require_once ( get_template_directory() . '/library/theme-options.php' );

	if ( function_exists( 'add_theme_support' ) ) { 
		add_theme_support( 'post-thumbnails' );

		// additional image sizes
		//add_image_size( 'for-masonry-thumb', 200, 9999 ); 
	}
}

add_action( 'after_setup_theme', 'fotowall_setup' );


// add Filters begin
function fotowall_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'fotowall_page_menu_args' );
// add Filters end

// functions

// get post thumbnail
// $height의 값이 -1 이면 자동조절
function fw_get_post_image_url($width = 100, $height = -1) {
	global $post;

	$result = '';

	if ( $images = get_children(array(
						'post_parent' => $post->ID,
						'post_type' => 'attachment',
						'numberposts' => 1,
						'post_mime_type' => 'image',))) {

		foreach( $images as $image ) {
			list($source_width, $source_height) = @getimagesize( $image->guid );	

			if($height == -1) {
				$height = floor($width * ($source_height / $source_width));
			}

			$new_filename = Func::get_filename($image->guid, false).'_'.$width.'x'.$height.'.jpg';

			$thumbnail = new Thumbnail;
			$to = FOTOWALL_LIBRARY.'/cache/'.$new_filename;
			
			//if(file_exists($to)) {
			//} else {						
				$thumbnail->resize($image->guid, $to, $width, $height);
			//}

			echo '<img src="'.get_bloginfo('template_directory').'/library/cache/'.$new_filename.'" />';
		}		
	} 

	if(empty($result)) {
	}

	return $ressult;
}

?>