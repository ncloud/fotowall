<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	global $view_styles;

	register_setting( 'fotowall_options', 'fotowall_theme_options', 'theme_options_validate' );

	$view_styles = array(
		'gallery' => array(
			'value' =>	'gallery',
			'label' => __( 'Gallery', 'fotowall' )
		),
		'masonry' => array(
			'value' =>	'masonry',
			'label' => __( 'Masonry', 'fotowall' )
		),
		'mosaic' => array(
			'value' => 'mosaic',
			'label' => __( 'Mosaic', 'fotowall' )
		),
		'post' => array(
			'value' => 'post',
			'label' => __( 'Post', 'fotowall' )
		)
	);
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_theme_page( __( 'Options', 'fotowall' ), __( 'Theme Options', 'fotowall' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
 * Create arrays for our select and radio options
 */

/**
 * Create the options page
 */
function theme_options_do_page() {
	global $view_styles;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . wp_get_theme() . __( ' Theme Options', 'fotowall' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Options saved', 'fotowall' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'fotowall_options' ); ?>
			<?php $options = get_option( 'fotowall_theme_options' ); ?>

			<table class="form-table">				
				<?php
				/**
				 * A fotowall select input option
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Default view style', 'fotowall' ); ?></th>
					<td>
						<select name="fotowall_theme_options[default_view_style]">
							<?php
								$selected = $options['default_view_style'];
								$p = '';
								$r = '';

								foreach ( $view_styles as $option ) {
									$label = $option['label'];
									if ( $selected == $option['value'] ) // Make default first in list
										$p = "\n\t<option style=\"padding-right: 10px;\" selected='selected' value='" . esc_attr( $option['value'] ) . "'>$label</option>";
									else
										$r .= "\n\t<option style=\"padding-right: 10px;\" value='" . esc_attr( $option['value'] ) . "'>$label</option>";
								}
								echo $p . $r;
							?>
						</select>
					</td>
				</tr>
			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'fotowall' ); ?>" />
			</p>
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
	global $view_styles;

	// Our select option must actually be in our array of select options
	if ( isset($input['default_view_style']) && ! array_key_exists( $input['default_view_style'], $view_styles ) )
		$input['default_view_style'] = null;

	return $input;
}

// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/