<?php
	class Func {
		function get_filename($fullname, $ext = true) {
			$filename = substr($fullname, strrpos($fullname,'/') + 1);
			if(!$ext) {
				return substr($filename,0,strrpos($filename,'.'));
			}
			return $filename;
		}
	}
?>