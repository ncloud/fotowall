<?php
class Thumbnail {
	var $types = array(
				1 => 'GIF',
				2 => 'JPG',
				3 => 'PNG',
				4 => 'SWF',
				5 => 'PSD',
				6 => 'BMP',
				7 => 'TIFF',
				8 => 'TIFF',
				9 => 'JPC',
				10 => 'JP2',
				11 => 'JPX',
				12 => 'JB2',
				13 => 'SWC',
				14 => 'IFF',
				15 => 'WBMP',
				16 => 'XBM'
			);

	function resize($from, $to, $w, $h, $type='auto') {
		list($org_w, $org_h, $org_img_type, $img_sizes) = GetImageSize($from);
		if($w > $org_w) $w = $org_w;
		if($h > $org_h) $h = $org_h;

		$ni = imageCreateTrueColor($w, $h);

		$white = imagecolorallocate($ni, 255,255,255); 
		imagefilledrectangle( $ni, 0, 0, $w, $h, $white );

		if($type == 'auto') {
			$im = $this->createImage($from, $org_img_type);
		} else {
			$im = $this->createImageByType($from, $type);
		}

		imagepalettecopy($ni,$im);

		imagecopyresampled(
		  $ni, $im,             // destination, source
		  0, 0, 0, 0,           // dstX, dstY, srcX, srcY
		  $w, $h,				// dstW, dstH
		  $org_w, $org_h);    // srcW, srcH

		$t = $this->getFileType($to);
		if($t == "gif") {
			$res = ImageGIF($ni, $to);
		} else if($t == "png") {
			$res = ImagePNG($ni, $to);
		} else if($t == "jpg" || $t == 'jpeg') {
			$res = ImageJPEG($ni, $to, 100);
		} else {
			$res = ImageJPEG($ni, $to, 100);
		}

		 return $to;
	}


	function resizeBaseWidth($from, $to, $w,$type='auto') {
		list($org_w, $org_h, $org_img_type, $img_sizes) = GetImageSize($from);
		if($w > $org_w) $w = $org_w;
		$h = $w * ($org_h / $org_w);

		$ni = imageCreateTrueColor($w, $h);

		// �Ͼ�� ĥ�ϱ�
		$white = imagecolorallocate($ni, 255,255,255); 
		imagefilledrectangle( $ni, 0, 0, $w, $h, $white );

		if($type == 'auto') {
			$im = $this->createImage($from, $org_img_type);
		} else {
			$im = $this->createImageByType($from, $type);
		}
		imagepalettecopy($ni,$im);
		
		imagecopyresampled(
		  $ni, $im,             // destination, source
		  0, 0, 0, 0,           // dstX, dstY, srcX, srcY
		  $w, $h,				// dstW, dstH
		  $org_w, $org_h);    // srcW, srcH

		$t = $this->getFileType($to);
		if($t == "gif") {
			$res = ImageGIF($ni, $to);
		} else if($t == "png") {
			$res = ImagePNG($ni, $to);
		} else if($t == "jpg" || $t == 'jpeg') {
			$res = ImageJPEG($ni, $to, 100);
		} else {
			$res = ImageJPEG($ni, $to, 100);
		}

		 return $to;

	}

	function crop($from, $to, $w, $h,$type='auto') {		
		list($org_w, $org_h, $org_img_type, $img_sizes) = GetImageSize($from);
		if($w > $org_w) $w = $org_w;
		if($h > $org_h) $h = $org_h;
		
		$ni = imageCreateTrueColor($w, $h);

		// �Ͼ�� ĥ�ϱ�
		$white = imagecolorallocate($ni, 255,255,255); 
		imagefilledrectangle( $ni, 0, 0, $w, $h, $white );		
		
		if($type == 'auto') {
			$im = $this->createImage($from, $org_img_type);
		} else {
			$im = $this->createImageByType($from, $type);
		}
		imagepalettecopy($ni,$im);
	
		$x = 0;
		$y = 0;
		
		if($h > $org_h) {
			$org_h = round($h * ($org_w / $w));
		} else {
			$h = round($org_h * ($w / $org_w));
		}
	
		imagecopyresampled(
		  $ni, $im,             // destination, source
		  0, 0, 0, 0,           // dstX, dstY, srcX, srcY
		  $w, $h,				// dstW, dstH
		  $org_w, $org_h);    // srcW, srcH
		
		$t = $this->getFileType($to);
		if($t == "gif") {
			$res = ImageGIF($ni, $to);
		} else if($t == "png") {
			$res = ImagePNG($ni, $to);
		} else if($t == "jpg" || $t == 'jpeg') {
			$res = ImageJPEG($ni, $to, 100);
		} else {
			$res = ImageJPEG($ni, $to, 100);
		}

		 return $to;
	}

	function cropCenter($from, $to, $w, $h,$type='auto') {		
		list($org_w, $org_h, $org_img_type, $img_sizes) = GetImageSize($from);
		if($w > $org_w) $w = $org_w;
		
		$w_fix = $w;
		$h_fix = $h;

		$ni = imageCreateTrueColor($w, $h);

		if($type == 'auto') {
			$im = $this->createImage($from, $org_img_type);
		} else {
			$im = $this->createImageByType($from, $type);
		}
		imagepalettecopy($ni,$im);
			
		$x = 0;
		$y = 0;
		$org_x = 0;
		$org_y = 0;

		if($h > $org_h) {
			$org_h = round($h * ($org_w / $w));
		} else {
			$h = round($org_h * ($w / $org_w));
		}
		if($w > $org_w) {
			$org_w = round($w * ($org_h / $h));
		} else {
			$w = round($org_w * ($h / $org_h));
		}

		$x = round(($w_fix / 2) - ($w / 2));
		$y = round(($h_fix / 2) - ($h / 2));

		imagecopyresampled(
		  $ni, $im,						// destination, source
		  $x, $y, $org_x, $org_y,       // dstX, dstY, srcX, srcY
		  $w, $h,						// dstW, dstH
		  $org_w, $org_h);				// srcW, srcH
		
		$t = $this->getFileType($to);
		if($t == "gif") {
			$res = ImageGIF($ni, $to);
		} else if($t == "png") {
			$res = ImagePNG($ni, $to);
		} else if($t == "jpg" || $t == 'jpeg') {
			$res = ImageJPEG($ni, $to, 100);
		} else {
			$res = ImageJPEG($ni, $to, 100);
		}

		 return $to;
	}

	function cropResize($from, $to, $w, $h,$type='auto') {		
		list($org_w, $org_h, $org_img_type, $img_sizes) = GetImageSize($from);
		if($w > $org_w) $w = $org_w;
		
		$ni = imageCreateTrueColor($w, $h);

		if($type == 'auto') {
			$im = $this->createImage($from, $org_img_type);
		} else {
			$im = $this->createImageByType($from, $type);
		}
		imagepalettecopy($ni,$im);
		
		if($org_h > $org_w) {
			if($h > $org_h) {
				$org_h = round($h * ($org_w / $w));
			} else {
				$h = round($org_h * ($w / $org_w));
			}
		} else {
			if($w > $org_w) {
				$org_w = round($w * ($org_h / $h));
			} else {
				$w = round($org_w * ($h / $org_h));
			}
		}
		 
		imagecopyresampled(
		  $ni, $im,             // destination, source
		  0, 0, 0, 0,           // dstX, dstY, srcX, srcY
		  $w, $h,				// dstW, dstH
		  $org_w, $org_h);    // srcW, srcH
		
		$t = $this->getFileType($to);
		if($t == "gif") {
			$res = ImageGIF($ni, $to);
		} else if($t == "png") {
			$res = ImagePNG($ni, $to);
		} else if($t == "jpg" || $t == 'jpeg') {
			$res = ImageJPEG($ni, $to, 100);
		} else {
			$res = ImageJPEG($ni, $to, 100);
		}

		 return $to;
	}
	
	private function createImage($from, $orgType = '') {
		if(empty($orgType)) {
			$t = $this->getFileType($from);
		} else {
			$t = strtolower($this->types[$orgType]);
		}

		if($t == "gif")
			$im = ImageCreateFromGIF($from);
		else if($t == "png") 
			$im = ImageCreateFromPNG($from);
		else if($t == "jpg" || $t == "jpeg") 
			$im = ImageCreateFromJPEG($from);
		else if($t == 'bmp') {
			$im = imageCreateFromWBMP($from);
		}

		return $im;
	}

	private function createImageByType($from, $type) {
		if($type == "image/gif" || $type == "gif")
			$im = ImageCreateFromGIF($from);
		else if($type == "image/png" || $type == "image/x-png" || $type == "png") 
			$im = ImageCreateFromPNG($from);
		else if($type == "image/jpeg" || $type == "image/pjpeg" || $type == "jpg" || $type == "jpeg") 
			$im = ImageCreateFromJPEG($from);	
		else if($type == "image/bmp" || $type == "bmp") 
			$im = imageCreateFromWBMP($from);

		return $im;
	}

	function getFileType($filename) {
		return strtolower(substr($filename, strrpos($filename,".")+1));
	}
}

?>