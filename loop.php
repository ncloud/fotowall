
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'fotowall' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'fotowall' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>

<?php 
	global $theme_options; 

	$classes = array();

	if(is_single())
		$classes[] = 'post_view_style_single';
	else
		$classes[] = 'post_view_style_' . $theme_options['default_view_style'];
?>

<div id="post_content" class="<?php echo implode(' ', $classes);?>">
<?php 
		if(is_single()) {
			the_post();
			get_template_part('content');
		} else {
			get_template_part('loop-' . $theme_options['default_view_style']); 
		}
?>
</div>

<?php if (  $wp_query->max_num_pages > 1 ) : ?>
	<div id="nav-below" class="navigation">
		<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'fotowall' ) ); ?></div>
		<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'fotowall' ) ); ?></div>
	</div><!-- #nav-below -->
<?php endif; ?>