
(function( window, $, undefined ){
   $.Tile = function( options, element ){
    this.element = $( element );

    this._create( options );
    this._init();
   };
  
  $.Tile.settings = {
    gutterWidth: 0,
    autoHeight :true, 
    containerStyle: {
      position: 'relative'
    },
    tileStyle: {
   	  position: 'absolute'
    }
  };
  
  $.Tile.prototype = {
    _create : function( options ) {
     this.options = $.extend( true, {}, $.Tile.settings, options );
     this.datas = {lastWidth : 0, tops: null, lastColIndex : 0, lastRowIndex : 0, ignoreCols : {}};
     
     this.element.css(this.options.containerStyle);
      //this.options.itemSelector
    },
    
    _init : function( compulsion, callback ) {
      compulsion = !compulsion ? false : compulsion;

      var wrapWidth = this.element.parent().width();
      if(wrapWidth == 0) return;
      
      var columnCount = Math.floor((wrapWidth - this.options.gutterWidth) / (this.options.gutterWidth + this.options.columnWidth))
	  var mainWidth = (columnCount * (this.options.gutterWidth + this.options.columnWidth)) - this.options.gutterWidth;
	  var mainHeight = 0;
	  var self = this;
	  
	  var i = 0; // col
	  var i2 = 0; // row
	  var left = 0;	  
	  var tops = [];
	  
	  var $items = this.element.find(this.options.itemSelector);
	  
      if(!compulsion && this.datas.lastWidth == wrapWidth) { // Added, Appended
	  	 $items = $items.filter(':not(.do_tile)');
	  	 
	  	tops = this.datas.tops;
	  	i = this.datas.lastColIndex;
	  	i2 = this.datas.lastRowIndex;
	  	
	  	left = this.datas.lastColIndex * (this.options.gutterWidth + this.options.columnWidth);
	  } else {
	  	for(var s = 0; s < columnCount ; s++) tops[s] = 0;
	  }
	  	  
	  if($items.length > 0) {
	      var ignoreCols = [];
	      			
	      $items.each(function(index, item) {
	      	var classes = '';
	      	$.each(item.className.split(" "), function(i, className) {
	      		var c = className.substr(0,4);
	      		if(c == 'row_' || c == 'col_') return;
	      		
	      		classes = classes + " " + className;
	      	});
	      	item.className = classes;
	  
	      	var $item = $(item);
	      	$item.css('min-height', '0');
	      	 
	      	if(i >= columnCount) {
	      		i = 0;
	      		left = 0;
	      		
	      		if(self.options.autoHeight) { // 각 열의 높이를 고정하도록 ..
	      			var max_height = 0;
	      			var $line_items = self.element.find(".row_" + i2);
	      			
	      			$line_items.each(function(sub_index, sub_item) {
	      				if(typeof(ignoreCols[sub_index]) != 'undefined' && ignoreCols[sub_index]) return;

	      				if(typeof(self.datas.ignoreCols[i2+"_"+sub_index]) != 'undefined' && self.datas.ignoreCols[i2+"_"+sub_index]) {
	      					ignoreCols[sub_index] = true;
	      					return;
	      				}
	      				 
	      				var now_height = $(sub_item).height();
	      				if(now_height > max_height) max_height = now_height;
	      			});
	      			
	      			$line_items.each(function(sub_index, sub_item) {
	      				if(typeof(ignoreCols[sub_index]) != 'undefined' && ignoreCols[sub_index]) {
	      					return;
	      				} 

						var $sub_item = $(sub_item);
	      				tops[sub_index] -= $sub_item.height();
	      				$sub_item.css('min-height',max_height + 'px');
	      				tops[sub_index] += $sub_item.height();
	      			});
	      		}
	      		
	      		i2 ++;
	      	}
	      	
	      	$item.css(self.options.tileStyle).css({left:left + 'px', top:tops[i] + 'px'}).addClass('row_' + i2).addClass('col_' + i);
	      	left += self.options.columnWidth + self.options.gutterWidth;
	      	tops[i] += $item.height() + parseInt($item.css('margin-top')) + parseInt($item.css('margin-bottom'));
	      	
	      	if(tops[i] > mainHeight) mainHeight = tops[i];
	
	      	i ++;
	      }).addClass('do_tile');
	      
	      this.datas.lastWidth = wrapWidth;
	      this.datas.tops = tops;
	      this.datas.lastColIndex = i;
	      this.datas.lastRowIndex = i2;
	
		  this.element.width(mainWidth).height(mainHeight);
		}
    },
    
    __getIndex: function($tile) {
    	var col_index = -1;
    	var row_index = -1;
      	$.each($tile.get(0).className.split(" "), function(i, className) {
      		var c = className.substr(0,4);
      		if(c == 'col_') col_index = parseInt(className.substr(4));
      		if(c == 'row_') row_index = parseInt(className.substr(4));
      	});
      	
      	return {col:col_index, row:row_index};
    },
    
    ignoreCols: function($tile) {
      	var index = this.__getIndex($tile);
		this.datas.ignoreCols[index.row + "_" + index.col] = true;
			
    	this.reloadCol(index.col);
    },
    
    removeIgnoreCols: function($tile) {
      	var index = this.__getIndex($tile);
    	this.datas.ignoreCols[index.row + "_" + index.col] = false;
    	
    	this.reloadCol(index.col);
    },
    
    reload : function() {
    	this._init(true);
    },
    
    
    reloadCol : function(index) {
    	var self = this;
    	
    	var top = 0;
    	var $item = null;
    	var work = false;
    	
    	this.element.find('.col_' + index).each(function(i, item) {
			if(typeof(self.datas.ignoreCols[i+"_"+index]) != 'undefined' && self.datas.ignoreCols[i+"_"+index]) {
				work = true;
			}
			
    		$item = $(item);
    		
			if(work) {
				$item.css('min-height', '0');
    		}
    		
    		$item.css('top', top + 'px');
    		top += $item.height() + (parseInt($item.css('margin-top')) + parseInt($item.css('margin-bottom')));
    	});	      	
    	
    	this.datas.tops[index] = top;
    	
    	var maxHeight = 0;
    	$.each(this.datas.tops, function(i, top) {
    		if(top > maxHeight) maxHeight = top;
    	});
    	
    	this.element.height(maxHeight);
    },
    
    reloadRow : function(index) {
    	// TODO: 특정 인덱스의 열 리로드
    },
    
    appended : function() {
    }
  };
  
  $.fn.tile = function( options ) {
	   if ( typeof options === 'string' ) {
	      var args = Array.prototype.slice.call( arguments, 1 );
	
	      this.each(function(){
	        var instance = $.data( this, 'tile' );
	        if ( !instance ) {
	          return;
	        }
	        if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {
	          return;
	        }
	        instance[ options ].apply( instance, args );
	      });
	    } 
		else {
	      this.each(function() {
	        var instance = $.data( this, 'tile' );

	        if ( instance && instance.option ) {
	          // apply options & init
	          instance.option( options || {} );
	          instance._init();
	        } else {
	          // initialize new instance
	          $.data( this, 'tile', new $.Tile( options, this ) );
	        }
	      });
	    }
	    return this;
  };
})( window, jQuery );