<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php

	global $page, $paged, $theme_options;

	wp_title( ' - ', true, 'right' );
	
	// 블로그 이름 

	bloginfo( 'name' );

	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " - $site_description";

	if ( $paged >= 2 || $page >= 2 )
		echo ' - ' . sprintf( __( 'Page %s', 'fotowall' ), max( $paged, $page ) );

	$theme_options = get_option('fotowall_theme_options');

	?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/styles/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/styles/bootstrap-simple.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel='stylesheet' id='flow-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Dosis:400,800,700,600,500,300,200|Open+Sans:400,800,800italic,700italic,700,600italic,600,400italic,300italic,300|Lato:400,700,400italic,900,300italic,300,100,700italic&#038;subset=latin,latin-ext' type='text/css' media='all' />

<?php
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	
	// 스크립트 추가
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/scripts/bootstrap.min.js', 'jquery', null, false);
	wp_enqueue_script('jquery.imagesloaded', get_template_directory_uri() . '/scripts/jquery.imagesloaded.js', 'jquery', null, false);
	wp_enqueue_script('fotowall', get_template_directory_uri() . '/scripts/fotowall.js', 'jquery', null, false);
	
	// 필수로 넣어야 함
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
	<div id="header" class="header">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="navbar-inner">
				<div class="container">

					<button type="button" class="btn btn-navbar btn-menu" data-toggle="collapse" data-target=".nav-collapse">
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
		          	</button>

					<button type="button" class="btn btn-navbar btn-search">
						<span class="icon-search"></span>
					</button>

					<div id="search" class="pull-right">
						<?php get_search_form(); ?>
					</div><!-- #search -->

					<!--<a class="brand" href="<?php echo get_bloginfo('url');?>"><?php echo get_bloginfo( 'name' );?></a>-->
					<?php /* 메뉴 */
						/* 테마에서 메뉴를 선택했는지 확인 -> 선택하지 않았다면 기본 메뉴 */
						if(has_nav_menu( 'primary' )) {
							$menu = wp_nav_menu( array( 'walker' => new Bootstrap_Walker_Nav_Menu(), 'echo' => false, 'depth'=>2, 'container_class'=>'nav-collapse collapse', 'menu_class'=>'nav', 'theme_location' => 'primary' ) ); 
							$menu = str_replace('>' . __('Home') . '<', '><span class="home">' . __('Home') . '</span><', $menu);
						} else {
							$menu = '<div class="nav-collapse collapse">
										<ul class="nav">
											<li' . (is_front_page() ? ' class="active"' : '') . '><a href="' . get_bloginfo('url') . '"><span class="home">' . __('Home') . '</span></a></li>
										</ul>
									 </div>
							';
						}

						echo $menu;
					?>
				</div>
			</div>
		</div><!-- .navbar -->

	</div><!-- #header -->

	<div id="sub_header">
		<div class="container">
			<h2 class="site_title">
				<a href="<?php echo get_bloginfo('url');?>"><?php echo get_bloginfo('name'); ?></a>
			</h2>
			<?php
				if(!empty($site_description)) {
			?>
			<div class="site_description">
				<?php echo $site_description; ?>
			</div>
			<?php
				}
			?>
			<div class="clearfix"></div>
		</div>
	</div><!-- #sub_header-->
	
	<div id="main">